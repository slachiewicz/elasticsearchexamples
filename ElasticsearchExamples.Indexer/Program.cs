﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using ElasticsearchExamples.Model;
using Humanizer;
using Nest;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using NetTopologySuite.IO.Converters;
using Newtonsoft.Json.Converters;

namespace ElasticsearchExamples.Indexer
{
    internal class Program
    {
        private const int BulkSize = 50;
        private const string DefaultIndex = "suburbs";

        public static void Main(string[] args)
        {
            Console.Title = "Elasticsearch Examples";

            var client = CreateElasticClient();
            client.CreateIndex(indexDescriptor => SuburbMapping.CreateSuburbMapping(indexDescriptor, DefaultIndex));

            var filename = @"..\..\..\Data\SSC06aAUST_region.shp";

            using (var reader = new ShapefileDataReader(filename, GeometryFactory.Default))
            {
                var dbaseHeader = ReadShapefileFields(reader);
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();

                var suburbs = new List<Suburb>(BulkSize);
                var stopWatch = Stopwatch.StartNew();
                var count = 0;

                while (reader.Read())
                {
                    count++;
                    var attributes = new AttributesTable();

                    for (var i = 0; i < dbaseHeader.NumFields; i++)
                    {
                        var fieldDescriptor = dbaseHeader.Fields[i];
                        attributes.AddAttribute(fieldDescriptor.Name, reader.GetValue(i));
                    }

                    var suburb = new Suburb
                    {
                        Geometry = reader.Geometry, 
                        Name = attributes["NAME_2006"].ToString(), 
                        State = (AustralianState)Enum.Parse(typeof(AustralianState), attributes["STATE_2006"].ToString(), true), 
                        Id = int.Parse(attributes["SSC_2006"].ToString())
                    };

                    suburbs.Add(suburb);

                    if (suburbs.Count == BulkSize)
                    {
                        IndexSuburbs(client, suburbs);
                        Console.WriteLine("indexed {0} suburbs in {1}", count, stopWatch.Elapsed.Humanize(5));
                        suburbs.Clear();
                    }
                }

                IndexSuburbs(client, suburbs);
                Console.WriteLine("indexed {0} suburbs in {1}", count, stopWatch.Elapsed.Humanize(5));
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        private static BulkDescriptor CreateBulkDescriptor(IEnumerable<Suburb> suburbs)
        {
            return new BulkDescriptor()
                .IndexMany(suburbs, (descriptor, suburb) => descriptor.Document(suburb));
        }

        private static IElasticClient CreateElasticClient()
        {
            var connectionSettings = new ConnectionSettings(new Uri("http://localhost:9200"))
                .SetDefaultIndex(DefaultIndex)
                .SetConnectTimeout(1000)
                .SetPingTimeout(200)
                .SetJsonSerializerSettingsModifier(settings =>
                {
                    settings.Converters.Add(new GeometryConverter());
                    settings.Converters.Add(new CoordinateConverter());
                    settings.Converters.Add(new StringEnumConverter());
                });

            var client = new ElasticClient(connectionSettings);

            return client;
        }

        private static void DisableRefreshInterval(IElasticClient client)
        {
            var response = client.UpdateSettings(settings => settings.Index(DefaultIndex).RefreshInterval("-1"));

            if (response.Acknowledged)
            {
                Console.WriteLine("refresh interval disabled");
            }
        }

        private static void EnableRefreshInterval(IElasticClient client)
        {
            var response = client.UpdateSettings(settings => settings.Index(DefaultIndex).RefreshInterval("1s"));

            if (response.Acknowledged)
            {
                Console.WriteLine("refresh interval set to 1 second");
            }
        }

        private static void IndexSuburbs(IElasticClient client, IEnumerable<Suburb> suburbs)
        {
            var bulkDescriptor = CreateBulkDescriptor(suburbs);
            var response = client.Bulk(bulkDescriptor);

            if (!response.IsValid)
            {
                if (response.ServerError != null)
                {
                    Console.WriteLine(response.ServerError.Error);
                }

                foreach (var item in response.ItemsWithErrors)
                {
                    Console.WriteLine(item.Error);
                }
            }
        }

        private static DbaseFileHeader ReadShapefileFields(ShapefileDataReader reader)
        {
            var dbaseHeader = reader.DbaseHeader;
            Console.WriteLine("{0} Columns, {1} Records", dbaseHeader.Fields.Length, dbaseHeader.NumRecords);

            for (var i = 0; i < dbaseHeader.NumFields; i++)
            {
                var fieldDescriptor = dbaseHeader.Fields[i];
                Console.WriteLine("{0} : {1}", fieldDescriptor.Name, fieldDescriptor.DbaseType);
            }

            return dbaseHeader;
        }
    }
}