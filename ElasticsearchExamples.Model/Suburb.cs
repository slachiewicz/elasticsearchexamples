using GeoAPI.Geometries;

namespace ElasticsearchExamples.Model
{
    public class Suburb
    {
        public IGeometry Geometry { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public AustralianState State { get; set; }
    }
}