﻿var map;

function initMap() {
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: new google.maps.LatLng(-25.274398, 133.775136),
        zoom: 5
    });
}

function loadGeoJsonString(geoString) {
    var geojson = JSON.parse(geoString);
    map.data.addGeoJson(geojson);
    zoom(map);
}

/**
 * Update a map's viewport to fit each geometry in a dataset
 * @param {google.maps.Map} map The map to adjust
 */
function zoom(map) {
    var bounds = new google.maps.LatLngBounds();
    map.data.forEach(function (feature) {
        processPoints(feature.getGeometry(), bounds.extend, bounds);
    });
    map.fitBounds(bounds);
}

/**
 * Process each point in a Geometry, regardless of how deep the points may lie.
 * @param {google.maps.Data.Geometry} geometry The structure to process
 * @param {function(google.maps.LatLng)} callback A function to call on each
 *     LatLng point encountered (e.g. Array.push)
 * @param {Object} thisArg The value of 'this' as provided to 'callback' (e.g.
 *     myArray)
 */
function processPoints(geometry, callback, thisArg) {
    if (geometry instanceof google.maps.LatLng) {
        callback.call(thisArg, geometry);
    } else if (geometry instanceof google.maps.Data.Point) {
        callback.call(thisArg, geometry.get());
    } else {
        geometry.getArray().forEach(function (g) {
            processPoints(g, callback, thisArg);
        });
    }
}

function initEvents() {
    var mapContainer = document.getElementById('map-canvas');
    var popover = $('.popover');
    var features = $('#features');

    google.maps.event.addListener(map, 'click', getSuburbForLatLon);

    function updatePopoverPosition(event) {
        popover.css({ 'top': (event.offsetY + 20) + "px", 'left': (event.offsetX + 20) + "px" });
    }

    // provide a popover when mouse over a feature
    map.data.addListener('mouseover', function (event) {      
        popover.find('.popover-title').text(event.feature.getProperty('name'));
        popover.find('.popover-content').text(event.feature.getProperty('state'));        
        popover.show();

        $(mapContainer).on('mousemove', updatePopoverPosition);
    });

    map.data.addListener('mouseout', function (event) {
        popover.hide();
        $(mapContainer).off('mousemove', updatePopoverPosition);
    });

    // remove feature on click
    map.data.addListener('click', function (e) {
        var feature = e.feature;

        map.data.remove(feature);

        features.find('[data-id="' + feature.getProperty("id") + '"]').remove();

        map.data.toGeoJson(function (featureCollection) {
            if (featureCollection.features.length) {
                zoom(map);
            }
        });

    });

    // give each feature a different colour
    map.data.setStyle(function (feature) {
        var color = '#' + Math.floor(Math.random() * 16777216).toString(16);

        return {
            fillColor: color,
            strokeColor: color,
            strokeWeight: 1
        };
    });

    $('#clear').click(function() {
        map.data.forEach(function(feature) {
            map.data.remove(feature);
        });

        features.empty();
    });

    $(document).on('click', '.close', function() {
        var li = $(this).parents('li');
        var featureId = li.data('id');
        var feature = map.data.getFeatureById(featureId);

        map.data.remove(feature);
        li.remove();
    });

    function getSuburbForLatLon(e) {

        $.get('api/search', { lat: e.latLng.lat(), lon: e.latLng.lng() }, function (response) {

            response.features.forEach(function (feature) {

                // NetTopologySuite does not expose setting an id for a feature,
                // so set it here
                feature.id = feature.properties.id;

                features.append('<li data-id="' + feature.properties.id + '" class="col-md-2">' +
                    '<div class="thumbnail"> <a class="close" href="#">x</a>' +
                        '<div class="caption"><p>' + feature.properties.name + ', ' + feature.properties.state + '</p></div>' +
                    '</div>' +
                    '</li>');
            });

            map.data.addGeoJson(response);
            zoom(map);
        });
    }
}

google.maps.event.addDomListener(window, 'load', function () {
    initMap();
    initEvents();
});

$(function() {

    $('.typeahead').typeahead({
        minLength: 3,
        highlight: true
    },
    {
        name: 'my-dataset',
        displayKey: "name",
        source: source
    });

    function source(query, callback) {

        $.get('api/search/names', { q: query }, function(response) {
            callback(response);
        });

    }

});