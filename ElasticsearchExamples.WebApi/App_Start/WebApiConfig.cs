﻿using System.Net.Http.Formatting;
using System.Web.Http;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO.Converters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace ElasticsearchExamples.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
                );

            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Converters =
                    {
                        new StringEnumConverter(),
                        new ICRSObjectConverter(),
                        new FeatureCollectionConverter(),
                        new FeatureConverter(),
                        new AttributesTableConverter(),
                        new GeometryConverter(GeometryFactory.Default),
                        new GeometryArrayConverter(),
                        new CoordinateConverter(),
                        new EnvelopeConverter()
                    }
                }
            });
        }
    }
}
