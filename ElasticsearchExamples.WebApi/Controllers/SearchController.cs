﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Web.Http;
using ElasticsearchExamples.Model;
using Nest;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO.Converters;

namespace ElasticsearchExamples.WebApi.Controllers
{
    [RoutePrefix("api/search")]
    public class SearchController : ApiController
    {
        private readonly IElasticClient _client;

        public SearchController()
        {
            var connectionSettings = new ConnectionSettings(new Uri("http://localhost:9200"));
            connectionSettings
                .SetDefaultIndex("suburbs")
                .SetConnectTimeout(1000)
                .SetPingTimeout(200).SetJsonSerializerSettingsModifier(settings =>
                {
                    settings.Converters.Add(new GeometryConverter());
                    settings.Converters.Add(new CoordinateConverter());
                });

            _client = new ElasticClient(connectionSettings);
        }

        [Route("count")]
        public HttpResponseMessage GetCount()
        {
            var response = _client.Count<Suburb>();

            return response.IsValid
                ? Request.CreateResponse(response.Count)
                : Request.CreateErrorResponse(HttpStatusCode.InternalServerError, response.ServerError.Error);
        }

        [Route("count/{state}")]
        public HttpResponseMessage GetCount(AustralianState state)
        {
            var response = _client.Count<Suburb>(descriptor => descriptor
                .Query(query => query
                    .Filtered(filtered => filtered
                        .Filter(filter => filter
                            .Bool(boolean => boolean
                                .Must(must => must
                                    .Term(suburb => suburb.State, state.ToString().ToLowerInvariant())
                                )
                            )
                        )
                    )
                ));

            return response.IsValid
                ? Request.CreateResponse(response.Count)
                : Request.CreateErrorResponse(HttpStatusCode.InternalServerError, response.ServerError.Error);
        }

        [Route("names")]
        public HttpResponseMessage GetNames(string q)
        {
            var response = _client.Search<Suburb>(search =>
                search.Query(query =>
                    query.MultiMatch(match =>
                        match.OnFields(s => s.Name, s => s.State)
                             .Query(q)
                             .Type(TextQueryType.PhrasePrefix)
                             .Slop(1)
                             .Operator(Operator.Or)
                             .MaxExpansions(50)
                        )
                    )
                );

            //var response = _client.Search<Suburb>(search =>
            //    search.Query(query =>
            //        query.Bool(boolean =>
            //            boolean.Must(must =>
            //                must.MatchPhrase(match =>
            //                    match.OnField(s => s.Name)
            //                        .Query(q)
            //                        .Operator(Operator.Or)
            //                        .MinimumShouldMatch("1")
            //                        .PrefixLength(3)
            //                    ),
            //                must => must
            //                    .MatchPhrase(match =>
            //                        match.OnField(s => s.State)
            //                            .Query(q)
            //                            .Operator(Operator.Or)
            //                            .MinimumShouldMatch("1")
            //                            .PrefixLength(3)
            //                    )
            //                )
            //            )
            //        )
            //    );

            return response.IsValid
                ? Request.CreateResponse(response.Documents.Select(d => new { name = string.Format("{0}, {1}", d.Name, d.State), id = d.Id }))
                : Request.CreateErrorResponse(HttpStatusCode.InternalServerError, response.ServerError.Error);
        }

        [Route("")]
        public HttpResponseMessage GetSuburb(double lat, double lon)
        {
            var response = _client.Search<Suburb>(search => search
                .Query(query => query
                    .Filtered(filtered => filtered
                        .Filter(filter => filter
                            .GeoShapePoint(s => s.Geometry, geo => geo
                                .Coordinates(new[] { lon, lat })
                            )
                        )
                    )
                ));

            if (!response.IsValid)
            {
                Request.CreateErrorResponse(HttpStatusCode.InternalServerError, response.ServerError.Error);
            }

            var featureCollection = new FeatureCollection();
            var point = new Point(lon, lat);

            foreach (var suburb in response.Documents)
            {
                // Elasticsearch seems to sometimes return multipolygons that the point
                // is not contained within. Use NetTopologySuite to remove these.
                if (suburb.Geometry.Contains(point))
                {
                    var attributes = new AttributesTable();
                    attributes.AddAttribute("id", suburb.Id);
                    attributes.AddAttribute("name", suburb.Name);
                    attributes.AddAttribute("state", suburb.State);
                    var feature = new Feature(suburb.Geometry, attributes);
                    featureCollection.Add(feature);
                }
            }

            return Request.CreateResponse(featureCollection);
        }
    }
}