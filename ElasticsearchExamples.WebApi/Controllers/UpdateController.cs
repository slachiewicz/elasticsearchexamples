using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ElasticsearchExamples.Model;
using Nest;
using NetTopologySuite.IO.Converters;

namespace ElasticsearchExamples.WebApi.Controllers
{
    public class UpdateController : ApiController
    {
        private readonly IElasticClient _client;

        public UpdateController()
        {
            var connectionSettings = new ConnectionSettings(new Uri("http://localhost:9200"));
            connectionSettings
                .SetDefaultIndex("suburbs")
                .SetConnectTimeout(1000)
                .SetPingTimeout(200).SetJsonSerializerSettingsModifier(settings =>
                {
                    settings.Converters.Add(new GeometryConverter());
                    settings.Converters.Add(new CoordinateConverter());
                });

            _client = new ElasticClient(connectionSettings);
        }

        public async Task<HttpResponseMessage> Update(Suburb suburb)
        {
            var response = await _client.GetAsync<Suburb>(suburb.Id).ConfigureAwait(false);

            if (!response.Found)
            {
                var indexResponse = await _client.IndexAsync(suburb, desc => desc.Id(suburb.Id)).ConfigureAwait(false);
            }
        }
    }
}